WIP: SchemaConf
===============

SchemaConf is a Node.js library for parsing a INI-like configuration system,
and validating the result based on a pre-defined schema. This catches mistakes
in configuration. Since entries can be repeated, it can also be used as a
simple human-readable plain-text storage system, such as for fixtures.

**WIP WARNING: SchemaConf is mostly undocumented.**

SchemaConf is the file format used by the
[Whiteboard](http://whiteboard.michaelb.org/) project.

It was originally teased out of the [Scroll
Editor](https://bitbucket.org/michaelb/scrolleditor/) project, but has not yet
been fully dis-entangled.  So, while it's basically "all there", it is not
ready for public consumption, as it has has no public API or documentation.

Running tests
-------------

```
make test
```

Example SchemaConf files
------------------------

```ini
    [entry]
    id = 1
    fruit = banana
    # Multiline delims include EOF, ''', and """
    description : EOF
        A block
        of text
    EOF

    # The same header being repeated generates a list
    [entry]
    id = 2
    fruit = pear
    description  : '  allows whitespace  '

    [OtherStuff]
    # Using { and } allows for nested types (no recursion, though)
    multivar : {
        [thing1]
        var = thing
        [thing2]
        var2 = thing2
    }
```



