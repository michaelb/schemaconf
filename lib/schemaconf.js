"use strict";
var fs = require('fs');
var EventEmitter = require('events').EventEmitter;
var util = require('util');
var path = require('path');

var _ = require('underscore');

var parse = require('./clearconf').parse;
var confutils = require('./confutils');


var SchemaConf = function (path, extends_resolver) {
    this.path = path;
    this.parents = [];
    this.raw_string = null; // string form of read file
    this.raw_obj = null; // obj form of read file (ignores inheritance)
    this.data = null; // obj form of read file (with inheritance)

    this.extends_resolver = extends_resolver || null;
};

util.inherits(SchemaConf, EventEmitter);

SchemaConf.prototype.resolve_path = function (other_path) {
    /*
      Resolves a path relative to this path, for imports, etc.
    */
    var my_dir = path.dirname(this.path);
    return path.resolve(my_dir, other_path);
};

SchemaConf.prototype.load = function (callback) {
    // If called with 1 argument, have that be callback, otherwise extends_resolver
    callback = callback || function () {};
    var me = this;

    var get_extends_path = function (info) {
        if (info.path) {
            return me.resolve_path(info.path);
        }
        if (!me.extends_resolver) {
            throw new Error('Invalid extends for ' + me.path);
        }
        return me.extends_resolver(info);
    };

    var build_parents = function (extends_list, callback) {
        if (!extends_list || extends_list.length < 1) {
            return callback();
        }

        var done = _.after(extends_list.length, callback);

        for (var i in extends_list) {
            var info = extends_list[i];
            var sc = new SchemaConf(get_extends_path(info));
            me.parents.push(sc);
            sc.load(done);
        }
    };

    fs.readFile(this.path, function (err, data) {
        if (err) { me.emit("error", err); return; }
        me.raw_string = new String(data);
        me.data = parse(me.raw_string);

        build_parents(me.data['extends'], function () {
            // Parents property is ready, update based on data
            var data_list = _.map(me.parents, function (p) { return p.data; });
            data_list.unshift(me.data); // insert my data as base
            me.data = confutils.conf_extend(data_list); // properly extend data
            me.emit('loaded');
            callback();
        });
    });
};

exports.SchemaConf = SchemaConf;

