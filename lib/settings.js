"use strict";
var _ = require('underscore');
var confschema = require('./confschema');

/*
 *  plugin_settings_schema
 */
var plugin_settings_schema = {
    // test empty values
    settings: {
        singular: true,
        values: {
            namespace: true,
            title: false,
            tooltip: false,
        },
    },
    setting: {
        values: {
            name: true,
            type: {
                choices: confschema.Types,
                type: confschema.Types.choice,
                required: true,
            },
            title: false,
            tooltip: false,
            help: false,
            choices: {
                type: confschema.Types.wordlist,
                required: false,
            },
            required: {
                type: confschema.Types.bool,
                required: false,
            },
            default: false,
        },
    },
};

var _settings_schema;
var get_settings_schema = function () {
    if (!_settings_schema) {
        _settings_schema = new confschema.ConfSchema(plugin_settings_schema, true);
    }
    return _settings_schema;
};

var validate_settings_infos = function (infos) {
    var schema = get_settings_schema();
    return infos.map(_.bind(schema.validate, schema));
};
module.exports.validate_settings_infos = validate_settings_infos;

var get_schemadict_from_settings_info = function (info) {
    var schema = get_settings_schema();
    var validated = schema.validate(info);
    var values = {};
    validated.setting.forEach(function (value_info) {
        var type = confschema.Types[value_info.type];
        var d = {
            type: type,
            required: value_info.required || false,
        };

        if ("default" in value_info) {
            d['default'] = type.parse(value_info.default);
        }

        if ("choices" in value_info) {
            d['choices'] = value_info['choices'];
        }

        values[value_info.name] = d;
    });

    var result = {};
    result[validated.settings.namespace] = {
        required: false,
        singular: true,
        values: values
    };
    return result;
};

var get_schema_from_all_plugins_info = function (plugins_info_list) {
    /*
     * Generates the schema dict for a particular settings info from a plugin.
     * For example, a list of parsed entries like:
     *
     * - core/editor/settings.cfg
     * [settings]
     * namespace = core.editor
     *
     * [setting]
     * name = show_toolbar
     * type = bool
     *
     * - core/ui/settings.cfg
     * [settings]
     * namespace = core.ui
     *
     * [setting]
     * name = font_size
     * type = number
     *
     * Becomes a schema that can validate something like:
     *
     * [core.editor]
     * show_toolbar = false
     *
     * [core.ui]
     * font_size = 12
     */

    var result = {};
    plugins_info_list.forEach(function (schemadict) {
        _.extend(result, get_schemadict_from_settings_info(schemadict));
    });
    return result;
};

module.exports.get_schema_from_all_plugins_info =
               get_schema_from_all_plugins_info;

var SettingsDict = function () {
    this.dict = {};
    this.changed = false;
};

SettingsDict.from_conf = function (conf) {
    var settingsdict = new SettingsDict();
    for (var header in conf) {
        if (conf[header] !== 1) {
            throw "Conf error: must specify exactly 1 header for " + header;
        }
        var obj = conf[header][0]; // ignore dupes, for now
        for (var key in obj) {
            var k = obj[header + ':' + key];
            settingsdict.dict[k] = obj[key];
        }
    }
    return settingsdict;
};

/* ********************
 * Converts from a SettingsDict into a conf file, for output back
 */
SettingsDict.prototype.to_conf = function () {
    var conf = {};
    for (var k in this.dict) {
        var s = k.split(":"),
            header=s[0],
            key=s[1];
        if (!conf[header]) { conf[header] = [{}]; }
        conf[header][0][key] = this.dict[k];
    }
    return conf;
};

SettingsDict.prototype.get = function (namespace, name) {
    return this.dict[namespace + ":" + name];
};


SettingsDict.prototype.update = function (settingsdict) {
    // Layers local settings over global settings, is not considered "changed"
    // in this case
    _.extend(this.dict, settingsdict.dict);
};

SettingsDict.prototype.set = function (namespace, name, value) {
    this.changed = true;
    this.dict[namespace + ":" + name] = value;
};



module.exports.SettingsDict = SettingsDict;
module.exports.settings_schema = plugin_settings_schema;

