"use strict";
var _ = require('underscore');




exports.conf_extend = function (datas) {
    /**
        Extends a list of Objects, similar to underscore's _.extend, with the
        exception that it assumes all data in the objects are lists, and so the
        lists are concatenated as opposed to over-written.
    **/
    var result = {};
    for (var i in datas) {
        var lore = datas[i];
        for (var key in lore) {
            if (!(key in result)) {
                result[key] = [];
            }

            Array.prototype.push.apply(result[key], lore[key]);
        }
    }
    return result;
};





