"use strict";
var fs   = require('fs');
var clearconf = require('./clearconf');
var confschema = require('./confschema');
var _ = require('underscore');

// Utility wrapper functions for loading and writing conf files
module.exports.load = function (options, callback) {
    var opts = _.extend({
        skip_defaults: false,
        schema: null,
        ignore_error: false,
        validate_empty: true,
    }, options);

    fs.readFile(options.path, function (err, data) {
        var loaded;
        if (err) {
            if (opts.ignore_error) {
                loaded = {};
            } else {
                throw new Error(conf_path + " reading conf " + err);
            }
        } else {
            loaded = clearconf.parse(data + '');
            if (!loaded) {
                throw new Error("Parsing error " + conf_path);
            }
        }

        // Apply schema, if applicable
        if (opts.schema && (!err || opts.validate_empty)) {
            if (_.isObject(opts.schema)) {
                opts.schema = new confschema.ConfSchema(opts.schema);
            }

            // now is a schema
            loaded = opts.schema.validate(loaded);

            if (!opts.skip_defaults) {
                opts.schema.apply_defaults(loaded);
            }
        }

        // Call back with validated data
        callback(loaded);
    });
};


