"use strict";
var _ = require('underscore');
_.str = require('underscore.string');

var taglex = require('taglex');

var make_ruleset = function () {
    // Compiles regexps for a Lexer to lex the conf file
    var ruleset = new taglex.RuleSet({
        normalizer: function (token) {
            return token.replace(/[\r\n\s]*\n[\r\n\s]*/g, "\n");
        },
    });

    // [Header]
    ruleset.add("[", taglex.ROOT, "section_title");
    ruleset.add("[", "conf", "section_title");
    ruleset.add("]", "section_title", "conf");

    // # comment, both in root and in a conf
    ruleset.add("#", taglex.ROOT, "root_comment");
    ruleset.add("\n", "root_comment", taglex.ROOT);
    ruleset.add("#", "conf", "comment");
    ruleset.add("\n", "comment", "conf");

    // assignment = value
    ruleset.add("=", "conf", "literal_unquoted");
    ruleset.add("\n", "literal_unquoted", "conf");

    // assignment : ''' \n value \n '''
    ruleset.add(":", "conf", "value");
    // opening
    ruleset.add("'", "value",   "sl_literal_s");
    ruleset.add('"', "value",   "sl_literal_d");
    ruleset.add("'''\n", "value", "ml_literal_s", "'''\\s*\\n");
    ruleset.add('"""\n', "value", "ml_literal_d", '"""\\s*\\n');
    ruleset.add('EOF\n', "value", "ml_literal_e", "EOF\\s*\\n");
    ruleset.add('{\n', "value", "structure", "\\{\\s*\\n");

    // closing
    ruleset.add("'",   "sl_literal_s", "conf"); // single line does not swallow
    ruleset.add('"',   "sl_literal_d", "conf");
    ruleset.add("\n'''\n", "ml_literal_s", "conf", "\\s*\\n\\s*'''\\s*\\n\\s*");
    ruleset.add('\n"""\n', "ml_literal_d", "conf", '\\s*\\n\\s*"""\\s*\\n\\s*');
    ruleset.add('\nEOF\n', "ml_literal_e", "conf", '\\s*\\n\\s*EOF\\s*\\n\\s*');
    ruleset.add('\n}\n', "structure", "conf", '\\s*\\n\\s*\\}\\s*\\n\\s*');

    return ruleset;
};

exports.RULESET = make_ruleset();

exports.parse = function (text, obj) {
    obj = obj || {};
    var current_section = null;
    var attribute = null;
    var lexer = exports.RULESET.new_lexer();

    var check_attribute = function () {
        return attribute && _.str.clean(attribute).length > 0;
    };

    lexer.on('token', function (type, value) {
        if (type !== taglex.TEXT_NODE) {
            return; // Only check state, no need to look at tag tokens
        }

        switch (lexer.state) {
            case "section_title":
                // [Header]
                var section_title = _.str.clean(value);
                current_section = {};
                if (!(section_title in obj)) {
                    obj[section_title] = [];
                }
                obj[section_title].push(current_section);
                break;

            case "comment":
                break; // Ignore comments

            case "value":
                break; // Ignore (value is the text between : and quotes)

            case "conf":
                // Could be attribute name, could be extraneous characters
                if (check_attribute()) {
                    // Unconsumed attribute, means last value had no text node,
                    // set to empty string for now, need to better handle later
                    current_section[attribute] = "";
                    //throw new Error("Extraneous characters: " + attribute);
                }

                // attribute names always get trimmed
                attribute = _.str.clean(value);
                break;

            case "literal_unquoted":
                value = _.str.clean(value); // literals get trimmed
                // ...fall through...
            case "sl_literal_s":
            case "sl_literal_d":
            case "ml_literal_s":
            case "ml_literal_d":
            case "ml_literal_e":
                // Actual value of field
                if (!check_attribute()) {
                    throw new Error("Attribute name missing");
                }
                current_section[attribute] = value;
                attribute = null;
                break;

            case "structure":
                // Recursive value (note: still regular language, since it only
                // supports one level of nesting)
                if (!check_attribute()) {
                    throw new Error("Attribute name missing");
                }

                value += "\n" // append a newline character to allow for EOF at the end
                current_section[attribute] = exports.parse(value);
                attribute = null;
                break;
        }
    });

    // Parse and emit tokens inline
    lexer.write(text);

    return obj;
};



// I pulled this from another project of mine from a long time ago.
/*
Function: unparse

converts an object into the "clearconf" format used by styles
    {'section': {'variable 1': 'value 1'}}
    becomes...
    '[section]\nvariable 1=value 1
    and
    {'section': [{a:10}, {a:12}, {a:14}]}
    becomes...
    '[section]\na=10\n[section]\na=12\n[section]\na=14\n'
    multilined strings are formated properly.

Parameters:
    obj - the object to be converted.

Returns: A string in the clearconf format

*/

exports.unparse = function(obj){
    var MULTILINE_DELIMS = ['"""', "'''", 'EOF'];
    var str = "";
    var n = "\n";
    for(var section in obj){
        // Loop through each section
        var list = obj[section];

        // loop through each entry
        for(var i=0,entry; entry = list[i]; i++){
            str = str + "[" + section + "]\n";
            var n = "\n";
            for(var variable in entry){
                variable = variable + "";
                var value = entry[variable] + "";
                var operator = "=";
                // Check for illegal characters
                if(variable.indexOf("=")!==-1
                        || variable.indexOf(":")!==-1
                        || variable.indexOf("#")===0) {
                    throw new Error("Invalid characters in variable name.");
                }

                if (_.isObject(value)) { // new code
                    value = exports.unparse(value);
                    value = "{" + n + value + n + "}" + n;
                } else if(value.indexOf("\n")!==-1){
                    var delim, delims = [].concat(MULTILINE_DELIMS);
                    operator = ":";

                    // Find a free delimiter
                    do {
                        delim = delims.shift();
                        if (delims.length < 1) {
                            throw new Error("Could not find valid delim for (" + value + ")");
                        }
                    } while(value.indexOf(n + delim + n) !== -1)

                    value = delim + n + value + n + delim + n;
                }
                str = str + variable + operator + value + n;
            }
        }
    }
    return str;
};
