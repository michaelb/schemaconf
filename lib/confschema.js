"use strict";
/*
 * ConfSchema allows schemas to be specified for the conf format
 * used by le2.
 *
 * This file has decent unit tests, and should be fairly stable atm.
 */


var _ = require('underscore');
_.str = require('underscore.string');


var BaseError = function () {};
BaseError.prototype.toString = function () {
    return this.name + ": " + this.message;
};

var TypeError = function (m) { this.name = "TypeError"; this.message = m; };
TypeError.prototype = new BaseError;
var ValueError = function (m) { this.name = "ValueError"; this.message = m; };
ValueError.prototype = new BaseError;
var SchemaError = function (m) { this.name = "SchemaError"; this.message = m; };
SchemaError.prototype = new BaseError;
var ArgumentError = function (m) { this.name = "ArgumentError"; this.message = m; };
ArgumentError.prototype = new BaseError;

var Types = {
    bool: {
        stringify: function (v) {
            return    (v === true)  ? "true"
                    : (v === false) ? "false"
                    : new TypeError("requires bool value");
        },
        parse: function (s) {
            //s = s.lower();
            return    (s === "true")  ? true
                    : (s === "false") ? false
                    : new ValueError("requires 'true' or 'false'");
        }
    },

    string: {
        stringify: function (v) {
            return    _.isString(v)  ? v
                    : new ValueError("requires string");
        },
        parse: function (s) { return s; }
    },

    wordlist: {
        stringify: function (v) {
            return    !_.isArray(v)            ? TypeError()
                    : v.join(',').match(/\s*/) ? TypeError()
                    : "    "+v.join("    \n");
        },
        parse: _.str.words
    },

    choice: {
        opts: {
            choices: function (s) {
                var choices;
                if (_.isString(s)) {
                    choices = _.str.words(s);
                } else if (_.isObject(s)) {
                    choices = _.keys(s);
                } else {
                    return new SchemaError("choices not obj or str");
                }
                return    choices.length > 0  ? choices
                        : new SchemaError();
            },
        },
        stringify: function (v, opts) {
            v = v.toLowerCase();
            return    opts.choices.indexOf(v) ? v
                    : new TypeError("invalid choice");
        },
        parse: function (s, opts) {
            s = s.toLowerCase();
            return    opts.choices.indexOf(s) ? s
                    : new ValueError("invalid choice");
        }
    },

    number: {
        stringify: function (v) {
            return _.isNumber(v) ? (v+"")
                    : new TypeError("number required");
        },
        parse: function (s) {
            return    s.match(/^\s*\d+\s*$/)  ? parseInt(s)
                    : new ValueError("non-digit characters found");
        }
    },
};


var ConfSchema = function (schema, opts) {
    opts = opts || {};

    this.schema = schema;
    //this.throw_on_error = !opts.no_exceptions;
    this.use_default = opts.use_default;

    /////////////////////////// PREP OPTS
    for (var k in this.schema) {
        var scheme = this.schema[k];
        for (var vk in scheme.values) {
            var vv = scheme.values[vk];

            // Now create and assign opts
            if (_.isObject(vv) && vv.type.opts) {
                vv._type_opts = {};
                for (var opt_name in vv.type.opts) {
                    var opt_func = vv.type.opts[opt_name];
                    var opt_result = opt_func(vv);
                    if (opt_result.error_type) {
                        throw new SchemaError("invalid option " + vv);
                    }
                    vv._type_opts[opt_name] = opt_result;
                }
            }
        }
    }
    ///////////////////////////

    /*
    Schema is like:
    { "taginfo": {
            singular: true, required: true,
            values: {
                "superleveltag": {
                    type: Types.bool, required: true,
                    default: false, }
            } } }
    */
};

ConfSchema.prototype.apply_defaults = function (validated) {
    /*
     * Fills in given validated data with default values
     *
     * Is done in place
     */
    if (!validated) {
        throw new ArgumentError("apply_defaults must have true value arg.");
    }

    for (var key in this.schema) {
        var scheme = this.schema[key];
        if (!(key in validated)) {
            if (scheme.singular) {
                validated[key] = {};
            } else {
                validated[key] = [];
            }
        }

        for (var vkey in scheme.values) {
            var set = function (conf) {
                if (!(vkey in conf)) {
                    var def_val = scheme.values[vkey].default;
                    if (typeof def_val !== "undefined") {
                        conf[vkey] = def_val;
                    }
                }
            };

            if (scheme.singular) {
                set(validated[key], vkey);
            } else {
                for (var i in validated[key]) {
                    set(validated[key][i], vkey);
                }
            }
        }
    }
};

ConfSchema.prototype.get_vscheme = function (header, vkey) {
    /*
     * Gets schema info for a single value under a single header
     */
    var scheme = this.schema[header];
    var vscheme = scheme.values[vkey];

    if (vscheme === true || vscheme === false) {
        vscheme = { required: vscheme, type: Types.string };
    } else {
        if (!_.isObject(vscheme)) {
            throw new SchemaError("invalid value " + vkey);
        }

        //if (_.isString(vscheme.type)) {
        //    // type specified as string
        //    vscheme.type = Types[vscheme.type];
        //}
    }
    return vscheme;
};

ConfSchema.prototype.validate_value = function (header, conf_entry,
                                                obj, vkey, vkey2, vscheme) {
    /*
     * Validates a single value, attaching the value if successful to "obj"
     */
    var e = header + "->" + vkey;

    if (!(vkey in conf_entry)) {
        if (vscheme.required) {
            throw new ValueError("Header " + e + " required.");
        } else if ("default" in vscheme) {
            if (this.use_default) {
                obj[vkey] = vscheme.default;
                return true;
            }
        }
        return true;
    }

    // parse, and assign, adding in any opts if exist
    try {
        var value = vscheme.type.parse(conf_entry[vkey],
                                        vscheme._type_opts);
    } catch (err) {
        value = {
            exception: true,
            error_type: (err+''),
        }
    }

    //throw "ValueError: error while parsing " + e + ": " + err;

    if (value.error_type) {
        if (value.exception) {
            throw new ValueError("exception while parsing " + e +
                                ": " + value.error_type);
        } else {
            throw new ValueError("error while parsing " + e + ": "
                                + value.error_type);
        }
    }

    obj[vkey2] = value;
    return true;
};


ConfSchema.prototype.validate_entry = function (header, conf_entry) {
    /*
     * Parses a single conf entry according to schema, throwing an error if it
     * encounters a problem, and returns the normalized version
     */
    var parsed = {};
    var scheme = this.schema[header];

    //////////// validate fixed values
    for (var vkey in scheme.values) {
        var vscheme = this.get_vscheme(header, vkey);
        var res = this.validate_value(header, conf_entry,
                                      parsed, vkey, vkey, vscheme);
        if (res !== true) { return; }
    }

    //////////// validate loose values
    if (scheme.multiple) {
        for (var multiple_key in scheme.multiple) {
            var multiple_scheme = scheme.multiple[multiple_key];
            var criteria = null, clean = null;
            parsed[multiple_key] = {};

            if (multiple_scheme.prefix) {
                var p = multiple_scheme.prefix;
                criteria = function (s) { return s.indexOf(p) === 0; };
                clean = function (s) { return s.slice(p.length); };
            } else {
                // without prefix, avoid already encountered ones
                criteria = function (s) { return !(s in parsed); }
            }

            for (var vkey in conf_entry) {
                if (criteria(vkey)) {
                    var local_key = clean ? clean(vkey) : vkey;
                    var res = this.validate_value(header, conf_entry,
                        parsed[multiple_key], vkey, local_key, multiple_scheme);
                    if (res !== true) { return; }
                }
            }
        }
    }

    return parsed;
};

ConfSchema.prototype.validate = function (conf) {
    /*
     * Parses a conf file, and returns a nice, normalized version, while
     * checking it against the schema
     */
    this.__current_conf = conf; // used in error reporting

    var validated = {};
    var me = this;
    for (var key in this.schema) {
        var scheme = this.schema[key]
        if (!conf[key]) {
            if (scheme.required) {
                throw new ArgumentError(key + " required.");
            }
            continue;
        }

        var conf_values = conf[key];

        if (scheme.singular && conf_values.length !== 1) {
            throw new ArgumentError(key + " is not singular.");
        }

        var parsed_results = conf_values.map(function (conf_entry) {
                return me.validate_entry(key, conf_entry); });
        if (scheme.singular) {
            parsed_results = parsed_results[0];
        }

        validated[key] = parsed_results;
    }

    return validated;
};


ConfSchema.prototype.reverse = function (validated) {
    /*
     * Parses the nice normalized version, and returns a conf file,
     * checking it against the schema
     */
    if (!validated) {
        throw new ArgumentError("Didn't even get anything to reverse!");
    }

    var result = {};
    var me = this;
    for (var key in this.schema) {
        var scheme = this.schema[key]
        if (!validated[key]) {
            if (scheme.required) {
                throw new TypeError(key + " required.");
            }
            continue;
        }

        var conf_values = validated[key];

        if (scheme.singular) {
            if (!_.isObject(conf_values)) {
                throw new TypeError(key + " is not singular.");
            }
            conf_values = [conf_values]; // wrap as obj
        }

        var parsed_results = conf_values.map(function (conf_entry) {
                return me.reverse_entry(key, conf_entry); });

        result[key] = parsed_results;
    }

    return result;

};



ConfSchema.prototype.reverse_entry = function (header, conf_entry) {
    /*
     * Parses a single conf entry according to schema, throwing an error if it
     * encounters a problem, and returns the normalized version
     */
    var parsed = {};
    var scheme = this.schema[header];

    for (var vkey in scheme.values) {
        var vscheme = this.get_vscheme(header, vkey);
        var e = header + "->" + vkey;
        if (!(vkey in conf_entry)) {
            if (vscheme.required) {
                throw new ValueError("ValueError: " + e + " required.");
            }
            continue;
        }
        // parse, and assign, adding in any opts if exist
        try {
            var value = vscheme.type.stringify(conf_entry[vkey],
                                        vscheme._type_opts);
        } catch (err) {
            value = {
                exception: true,
                error_type: (err+''),
            }
        }

        if (value.error_type) {
            if (value.exception) {
                throw new ValueError("ValueError: exception while parsing " + e + ": " + value.error_type);
            } else {
                throw new ValueError("ValueError: error while parsing " + e + ": " + value.error_type);
            }
        }

        parsed[vkey] = value;
    }
    return parsed;
};




module.exports.ConfSchema = ConfSchema;
module.exports.Types  = Types;
