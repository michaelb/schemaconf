var schemaconf = require('./index');
var fs = require('fs');
var path = require('path');
var _ = require('underscore');

//var confschema = require('../lib/conf/confschema');
//var settings = require('../lib/conf/settings');
//var conffs = require('../lib/conf/conffs');

// aliases, as we clean up
var clearconf = schemaconf;
var settings = schemaconf;
var conffs = schemaconf;
var confschema = schemaconf;


exports.test_examples = function (test) {
    var mockery = require('mockery');
    mockery.enable();
    mockery.registerMock('schemaconf', schemaconf);
    mockery.warnOnUnregistered(false);
    var out = [];
    mockery.registerMock('sys', {
        print: function (s) { out.push(s); },
    });

    var check = function (name) {
        out = [];
        var example = require('./examples/' + name);
        test.deepEqual(out.join("").split("\n"), example.expected, 'Example ' + name);
    };

    //check('lexing_regular_language');
    //check('markdown');

    mockery.disable();
    test.done();
};


process.on('uncaughtException', function(err) {
    console.error(err.stack);
    console.error(err);
});

var PATH_PREFIX = path.join(__dirname, "testdata");

var NOEXCP = {
    no_exceptions: true,
    use_default: true,
};


var get_clearconf = function (name) {
    var data = fs.readFileSync(path.join(PATH_PREFIX, name + ".cfg"));
    result = clearconf.parse(data + "");
    return result;
};

var get_json = function (name) {
    var data = fs.readFileSync(path.join(PATH_PREFIX, name + ".cfg.json"));
    return JSON.parse(data + "");
};

var cmp_clearconf = function (test, name, modify, print) {
    modify = modify || function (a) { return a; };
    var cc = get_clearconf(name);
    cc = modify(cc);

    if (print) {
        console.log("result\n", JSON.stringify(cc, null, "    "));
    }
    var json = get_json(name);
    test.deepEqual(cc, json);
    var data = fs.readFileSync(path.join(PATH_PREFIX, name + ".cfg"));
    return clearconf.parse(data + "");
};

exports.test_clearconf = function (test) {
    // test basic stuff
    cmp_clearconf(test, "test_clearconf00");

    // test edge cases
    test.deepEqual(clearconf.parse(''), {});
    var res = { empty_header: [ {} ], another_empty_header: [ {} ] };
    test.deepEqual(clearconf.parse('[empty_header]\n[another_empty_header]\n'), res);

    test.done();
};


exports.test_example_theme = function (test) {
    // test some more edge cases
    cmp_clearconf(test, "test_exampletheme");
    test.done();
};

exports.test_assignment_syntax = function (test) {
    // test some more edge cases
    cmp_clearconf(test, "test_clearconf01");
    test.done();
};


exports.test_recursive = function (test) {
    // test recursive { } style structures
    cmp_clearconf(test, "test_recurse");
    test.done();
};

var SCHEMA = {
    // test empty values
    "header1": {
        required: true, singular: true,
        values: { nonexistant: {
                type: confschema.Types.number,
                required: false,
            } }
    },
    "header2": {
        required: true, singular: true, values: {
            value: {
                type: confschema.Types.number,
                required: true, },
            karma: {
                type: confschema.Types.string,
                required: true, },
            koala: {
                type: confschema.Types.choice,
                choices: 'vegetable animal mineral',
                //choices: {'vegetable': true, 'animal': true, 'mineral': true},
                required: true, }, }
    },
    "header3": { values: {
            value: { type: confschema.Types.number, required: true, }, }
    },
};

exports.test_clearconf_schema = function (test) {
    var schema = _.extend({}, SCHEMA);

    // test stuff
    cmp_clearconf(test, "test_schema00", function (cc) {
        // apply schema
        var s = new confschema.ConfSchema(schema, NOEXCP);
        return s.validate(cc);
    });

    var cc = get_clearconf("test_schema00");
    var s = new confschema.ConfSchema(schema, NOEXCP);
    var res = s.validate(cc);
    res.header1.nonexistant = 12;
    res.header2.value = 15;
    res.header2.koala = "vegetable";
    res.header3[0].value = 7;
    res = s.reverse(res);
    //console.log("result\n", JSON.stringify(res, null, "    "));
    var json = get_json("test_schema01");
    test.deepEqual(res, json);

    // Now, lets do a symmetric conversion back and forth

    // Generate a bigger schema
    schema['student'] = { 'required': true,
        values: { 'name': true, 'subject': false }};

    cmp_clearconf(test, "test_schema02", function (cc) {
        // apply schema
        var validated, res;
        var s = new confschema.ConfSchema(schema, NOEXCP);
        validated = s.validate(cc);
        res = s.reverse(validated);
        // back into string
        var stringified = clearconf.unparse(res);
        res = clearconf.parse(stringified);
        // now back to being validated
        validated = s.validate(res);
        // and reversed again
        res = s.reverse(validated);
        return res;
    });


    var Types = confschema.Types;
    // Test apply default features
    var def_schema = new confschema.ConfSchema({
        "h1": {
            required: false,
            singular: true,
            values: {
                "test1": { required: false, default: "def", type: Types.string, },
                "test2": { required: false, default: false, type: Types.bool, },
            }
        },
        "entry": {
            required: true,
            singular: false,
            values: {
                "var1": { required: false, default: "thing", type: Types.string, },
                "var2": { required: true, type: Types.bool, },
            }
        },
        "entry2": {
            required: false,
            singular: false,
            values: {
                "var1": { required: false, default: "thing", type: Types.string, },
                "var2": { required: true, type: Types.bool, },
            }
        }
    }, NOEXCP);
    var conf_data = {"entry": [ { var1: "custom value", var2: "false" }, { var2: "true" }, ]};
    var validated = def_schema.validate(conf_data);

    var expected_validated = {"entry": [ { var1: "custom value", var2: false },
                                         { var1: "thing", var2: true }, ]};
    test.deepEqual(validated, expected_validated);

    // now apply defaults
    def_schema.apply_defaults(validated);

    var expected_validated2 = { "h1": { test1: "def", test2: false },
                              "entry2": [],
                              "entry": [ { var1: "custom value", var2: false },
                                         { var1: "thing", var2: true }, ]};
    test.deepEqual(validated, expected_validated2);

    // Test edge cases
    var emptyschema = new confschema.ConfSchema({}, NOEXCP);
    var validated = emptyschema.validate({'any': {'damn': 'thing'}});
    test.deepEqual(validated, {});


    test.done();
};

exports.test_clearconf_schema_multiple = function (test) {
    //////////// test "multiple" directive
    var schema = { "test": {
        required: true, singular: true,
        values: { a: true },
        multiple: {
            numbers: {
                prefix: "_",
                type:   confschema.Types.number,
            },
            strings: {
                prefix: "$",
                type:   confschema.Types.string,
            },
        },
    } };

    cmp_clearconf(test, "test_schema03", function (cc) {
        // apply schema
        var s = new confschema.ConfSchema(schema, NOEXCP);
        return s.validate(cc);
    });
    test.done();
};

exports.test_settings_schema = function (test) {
    var Types = confschema.Types;
    var desired_settings_schema = {
        "core.editor": {
            "required": false,
            "singular": true,
            "values": {
                "show_toolbar": {
                    "type": Types.bool,
                    "required": true
                },
                "username": {
                    "type": Types.string,
                    "default": "unknown user",
                    "required": false
                },
            },
        },
        "core.ui": {
            "required": false,
            "singular": true,
            "values": {
                "font_size": {
                    "type": Types.number,
                    "default": 12,
                    "required": false,
                },
                "shadow": {
                    "choices": ["long", "medium", "short"],
                    "type": Types.choice,
                    "required": false,
                },
            },
        },
    };
    var plugin_settings_list = [
        get_clearconf("test_settings0"),
        get_clearconf("test_settings1"),
    ];

    var schema_dict = settings.get_schema_from_all_plugins_info(plugin_settings_list)
    test.deepEqual(schema_dict, desired_settings_schema);

    // Now, we actually parse something with this schema_dict, and test against
    // the expected result
    cmp_clearconf(test, "test_settings2", function (cc) {
        // apply schema
        var s = new confschema.ConfSchema(schema_dict, NOEXCP);
        return s.validate(cc);
    });

    // Now, we test completely default stuff
    schema_dict['core.editor']['values']['show_toolbar']['required'] = false;
    schema_dict['core.editor']['values']['show_toolbar']['default'] = true;

    var setdef = new confschema.ConfSchema(schema_dict);
    var cc = setdef.validate({});
    test.deepEqual(cc, {}); // ensure empty

    // now apply defaults
    setdef.apply_defaults(cc);
    var expected_defaults = {
        "core.editor": { "show_toolbar": true, "username": "unknown user" },
        "core.ui": { "font_size": 12 }
    };
    test.deepEqual(cc, expected_defaults); // ensure empty

    test.done();
};

exports.test_conffs = function (test) {
    conffs.load({
            path: path.join(PATH_PREFIX, "test_schema00.cfg"),
            schema: SCHEMA
        }, function (validated) {
            test.ok(_.isObject(validated));
            test.equal(validated.header2.value, 10);
            test.done();
        });
};


exports.test_inheritance = function (test) {
    var p = path.join(PATH_PREFIX, "test_inherit.cfg");
    var sc = new schemaconf.SchemaConf(p);

    // Test abspath
    var test_path = path.join(PATH_PREFIX, "something");
    test.equal(sc.resolve_path("./something"), test_path); // relative 1
    test.equal(sc.resolve_path("../testdata/something"), test_path); // relative 2
    test.equal(sc.resolve_path(test_path), test_path); // abs

    sc.on('loaded', function () {
        test.equal(sc.data.datum.length, 4);
        test.equal(sc.data.child_only.length, 1);
        test.equal(sc.data.parent_only.length, 1);
        test.equal(sc.data.other_parent_only.length, 1);

        test.deepEqual(sc.data.datum, [
            {number: 1}, {number: 2}, {number: 3}, {number: 4}, ]);

        test.done();
    });
    sc.load();
};


exports.test_inheritance_with_custom_resolver = function (test) {
    var p = path.join(PATH_PREFIX, "test_inherit_custom.cfg");
    // resolver here:
    var sc = new schemaconf.SchemaConf(p, function (info) {
        return path.join(PATH_PREFIX, 'test_inherit_parent' + info.number + '.cfg');
    });

    sc.on('loaded', function () {
        // Should have same data as the other
        test.equal(sc.data.datum.length, 4);
        test.equal(sc.data.child_only.length, 1);
        test.equal(sc.data.parent_only.length, 1);
        test.equal(sc.data.other_parent_only.length, 1);

        test.deepEqual(sc.data.datum, [
            {number: 1}, {number: 2}, {number: 3}, {number: 4}, ]);

        test.done();
    });
    sc.load();
};



