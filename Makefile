.PHONY: help test release

help:
	@echo "test - run tests headless"
	@echo "release - package and upload a release on npm"
	@echo "bump-and-push - run tests, lint, bump patch, push to git, and release on npm"

test:
	./node_modules/.bin/nodeunit tests.js

bump-and-push: test
	bumpversion patch
	git push
	git push --tags
	make release

release:
	npm publish
