var clearconf = require('./lib/clearconf');
exports.parse = clearconf.parse;
exports.unparse = clearconf.unparse;

// better API
exports.format = {
    parse: clearconf.parse,
    stringify: clearconf.unparse,
};

var conffs = require('./lib/conffs');
exports.load = conffs.load;
exports.fs = conffs;

var settings = require('./lib/settings');
exports.get_schema_from_all_plugins_info =
               settings.get_schema_from_all_plugins_info;


var confschema = require('./lib/confschema');
exports.Types      = confschema.Types;
exports.ConfSchema = confschema.ConfSchema;


var schemaconf = require('./lib/schemaconf');
exports.SchemaConf   = schemaconf.SchemaConf;
